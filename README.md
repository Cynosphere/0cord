# 0cord

Maintained pitch black CSS for Discord

Works fine as of December 10, 2018, tested on Discord Canary.

Use stylus to install to your browser, and use anything *but* BetterDiscord to install to your client. Keep in mind that client modding is a violation of Discord ToS and can get you banned.

Also on userstyles: https://userstyles.org/styles/163340/0cord-pitch-black-discord

## Screenshots

![A screenshot of discord with this CSS theme](https://awo.oooooooooooooo.ooo/i/hztmcuit.png)

## License

CC-BY-NC
